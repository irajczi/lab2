/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author irajczi
 *
 */
public class Celcius extends Temperature{
	public Celcius (float t)
	{
		super (t);
	}
	public String toString()
	{
		return "" + value + " C";
	}
	@Override
	public Temperature toCelcius() {
		return new Celcius (value);
	}
	@Override
	public Temperature toFahrenheit() {
		float times = value * 9;
		float divide = times / 5;
		float finish = divide + 32;
		return new Fahrenheit (finish);
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin (value + 273);
	}

}
