/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author irajczi
 *
 */
public class Fahrenheit extends Temperature{
	public Fahrenheit (float t)
	{
		super (t);
	}
	public String toString()
	{
		return "" + value + " F";
	}
	@Override
	public Temperature toCelcius() {
		float working = value - 32;
		float times = working * 5;
		float finish = times / 9;
		return new Celcius (finish);
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit (value);
	}
	@Override
	public Temperature toKelvin() {
		Temperature temp = this.toCelcius();
		return temp.toKelvin();
	}

}
