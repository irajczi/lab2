/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (irajczi): write class javadoc
 *
 * @author irajczi
 *
 */
public class Kelvin extends Temperature {
	public Kelvin (float t)
	{
		super (t);
	}
	
	public String toString ()
	{
		return "" + value + " K";
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toCelcius()
	 */
	@Override
	public Temperature toCelcius() {
		return new Celcius (value - 273);
	}

	/* (non-Javadoc)
	 * @see edu.ucsd.cs110w.temperature.Temperature#toFahrenheit()
	 */
	@Override
	public Temperature toFahrenheit() {
		Temperature temp = this.toCelcius();
		return temp.toFahrenheit();
	}
	
	@Override
	public Temperature toKelvin ()
	{
		return new Kelvin (value);
	}

}
