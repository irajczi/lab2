package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Celcius;
import edu.ucsd.cs110w.temperature.Temperature;

public class CelciusTests extends TestCase {
	private float delta = 0.001f;
	public void testCelcius ()
	{
		float value = 12.34f;
		Celcius temp = new Celcius (value);
		assertEquals (value, temp.getValue(), delta);
		
	}
	public void testCelciusToString ()
	{
		float value = 12.34f;
		Celcius temp = new Celcius (value);
		String string = temp.toString();
		String begin = "" + value;
		String end = " C";
		//Verify prefix
		assertTrue (string.startsWith(begin));
		//Verify suffix
		assertTrue (string.endsWith(end));
		//Verify middle
		int endIndex = string.indexOf(end);
		assertTrue (string.substring(0, endIndex).equals(begin));
	}
	public void testCelciustoCelcius ()
	{
		Celcius temp = new Celcius (0);
		Temperature convert = temp.toCelcius();
		assertEquals(0,convert.getValue(), delta);
	}
	public void testCelciustoFahrenheit ()
	{
		Celcius temp = new Celcius (0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(32, convert.getValue(), delta);
		temp = new Celcius (100);
		convert = temp.toFahrenheit();
		assertEquals (212, convert.getValue(), delta);
		
	}
	public void testCelciustoKelvin ()
	{
		Celcius temp = new Celcius (0);
		Temperature convert = temp.toKelvin();
		assertEquals(273, convert.getValue(), delta);
		temp = new Celcius (100);
		convert = temp.toKelvin();
		assertEquals (373, convert.getValue(), delta);
		
	}
}