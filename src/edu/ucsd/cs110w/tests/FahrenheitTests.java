package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Fahrenheit;
import edu.ucsd.cs110w.temperature.Temperature;

public class FahrenheitTests extends TestCase {
	private float delta = 0.001f;
	public void testCelcius ()
	{
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit (value);
		assertEquals (value, temp.getValue(), delta);
		
	}
	public void testFahreneheitToString ()
	{
		float value = 12.34f;
		Fahrenheit temp = new Fahrenheit (value);
		String string = temp.toString();
		String begin = "" + value;
		String end = " F";
		//Verify prefix
		assertTrue (string.startsWith(begin));
		//Verify suffix
		assertTrue (string.endsWith(end));
		//Verify middle
		int endIndex = string.indexOf(end);
		assertTrue (string.substring(0, endIndex).equals(begin));
	}
	public void testFahrenheittoFahrenheit ()
	{
		Fahrenheit temp = new Fahrenheit (0);
		Temperature convert = temp.toFahrenheit();
		assertEquals(0,convert.getValue(), delta);
	}
	public void testFahrenheittoCelcius ()
	{
		Fahrenheit temp = new Fahrenheit (32);
		Temperature convert = temp.toCelcius();
		assertEquals(0, convert.getValue(), delta);
		temp = new Fahrenheit (212);
		convert = temp.toCelcius();
		assertEquals (100, convert.getValue(), delta);
		
	}
	public void testFahrenheittoKelvin ()
	{
		Fahrenheit temp = new Fahrenheit (32);
		Temperature convert = temp.toKelvin();
		assertEquals (273, convert.getValue(), delta);
		temp = new Fahrenheit (212);
		convert = temp.toKelvin();
		assertEquals (373, convert.getValue(), delta);
	}

}
