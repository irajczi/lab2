package edu.ucsd.cs110w.tests;

import junit.framework.TestCase;
import edu.ucsd.cs110w.temperature.Kelvin;
import edu.ucsd.cs110w.temperature.Temperature;

public class KelvinTests extends TestCase {
	private float delta = 0.001f;
	public void testCelcius ()
	{
		float value = 12.34f;
		Kelvin temp = new Kelvin (value);
		assertEquals (value, temp.getValue(), delta);
		
	}
	public void testFahreneheitToString ()
	{
		float value = 12.34f;
		Kelvin temp = new Kelvin (value);
		String string = temp.toString();
		String begin = "" + value;
		String end = " K";
		//Verify prefix
		assertTrue (string.startsWith(begin));
		//Verify suffix
		assertTrue (string.endsWith(end));
		//Verify middle
		int endIndex = string.indexOf(end);
		assertTrue (string.substring(0, endIndex).equals(begin));
	}
	public void testKelvintoKelvin ()
	{
		Kelvin temp = new Kelvin (0);
		Temperature convert = temp.toKelvin();
		assertEquals(0,convert.getValue(), delta);
	}
	public void testKelvintoCelcius ()
	{
		Kelvin temp = new Kelvin (1);
		Temperature convert = temp.toCelcius();
		assertEquals(-272, convert.getValue(), delta);
		temp = new Kelvin (273);
		convert = temp.toCelcius();
		assertEquals (0, convert.getValue(), delta);
	}
	public void testKelvintoFahrenheit ()
	{
		Kelvin temp = new Kelvin (1);
		Temperature convert = temp.toFahrenheit();
		assertEquals (-457.6, convert.getValue(), delta);
		temp = new Kelvin (273);
		convert = temp.toFahrenheit();
		assertEquals (32, convert.getValue(), delta);
	}

}
